<?php 
    // Connect to the database using MySQLi
    function OpenCon() {
        $host = 'localhost';
        $username = 'root';
        $password = '';
        $database = 'employee_db';


        $conn = new mysqli($host, $username, $password, $database);


        if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
        }else{
            
            echo "Connection established successfully!<br>";
            return $conn;
        }
    }


    function CloseCon($conn)
    {
        $conn -> close();
    }
?>