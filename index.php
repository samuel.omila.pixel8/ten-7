<!DOCTYPE html>
<html>
<?php
    include 'dbconn.php';
    $conn = OpenCon();

    // CREATE
    if (isset($_POST['create'])) {
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $birthday = $_POST['birthday'];
        $address = $_POST['address'];

        $insertSql = "INSERT INTO employee (first_name, last_name, birthday, address) 
                    VALUES ('$first_name', '$last_name', '$birthday', '$address')";
        if ($conn->query($insertSql) === TRUE) {
            echo "Record created successfully.<br>";
        } else {
            echo "Error creating record: " . $conn->error . "<br>";
        }
    }

    // UPDATE
    if (isset($_POST['update'])) {
        $id = $_POST['update_id'];
        $new_first_name = $_POST['new_first_name'];
        $new_last_name = $_POST['new_last_name'];
        $new_birthday = $_POST['new_birthday'];
        $new_address = $_POST['new_address'];
    
        $sql = "UPDATE employee SET 
                      first_name = '$new_first_name',
                      last_name = '$new_last_name',
                      birthday = '$new_birthday',
                      address = '$new_address'
                      WHERE id = $id";
    
        if ($conn->query($sql) === TRUE) {
            echo "Record updated successfully.<br>";
        } else {
            echo "Error updating record: " . $conn->error . "<br>";
        }
    }

    // DELETE
    if (isset($_POST['delete'])) {
        $id = $_POST['delete_id'];

        $deleteSql = "DELETE FROM employee WHERE id = $id";
        if ($conn->query($deleteSql) === TRUE) {
            echo "Record deleted successfully.<br>";
        } else {
            echo "Error deleting record: " . $conn->error . "<br>";
        }
    }
?>
<head>
    <title>Employee CRUD</title>
</head>
<body>
    <h2>Create Employee</h2>
    <form method="POST">
        First Name: <input type="text" name="first_name"><br>
        Last Name: <input type="text" name="last_name"><br>
        Birthday: <input type="date" name="birthday"><br>
        Address: <input type="text" name="address"><br>
        <button type="submit" name="create">Create</button>
    </form>

    <h2>Employees</h2>
    <table border="1">
        <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Birthday</th>
            <th>Address</th>
            <th>Actions</th>
        </tr>
        <?php
        // READ
        $sql = "SELECT * FROM employee";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row["id"] . "</td>";
                echo "<td>";
                echo $row["first_name"];
                echo "</td>";
                echo "<td>" . $row["last_name"] . "</td>";
                echo "<td>" . $row["birthday"] . "</td>";
                echo "<td>" . $row["address"] . "</td>";
                echo "<td>";
                if (isset($_POST['cancel'])) {
                    unset($_POST['edit_id']);
                }
                if (isset($_POST['edit']) && $_POST['edit_id'] == $row["id"]) {
                    echo "<form method='POST'>";
                    echo "<input type='hidden' name='update_id' value='" . $row["id"] . "'>";
                    echo "First Name: <input type='text' name='new_first_name' value='" . $row["first_name"] . "'><br>";
                    echo "Last Name: <input type='text' name='new_last_name' value='" . $row["last_name"] . "'><br>";
                    echo "Birthday: <input type='date' name='new_birthday' value='" . $row["birthday"] . "'><br>";
                    echo "Address: <input type='text' name='new_address' value='" . $row["address"] . "'><br>";
                    echo "<button type='submit' name='update'>Update</button>";
                    echo "</form>";
                    echo "<form method='POST'><button type='submit' name='cancel'>Cancel</button></form>";
                } else {
                    echo "<form method='POST'>";
                    echo "<input type='hidden' name='edit_id' value='" . $row["id"] . "'>";
                    echo "<button type='submit' name='edit'>Edit</button>";
                    echo "</form>";
                    echo "<form method='POST'>";
                    echo "<input type='hidden' name='delete_id' value='" . $row["id"] . "'>";
                    echo "<button type='submit' name='delete'>Delete</button>";
                    echo "</form>";
                }
                
                echo "</td>";
                echo "</tr>";
            }
        } else {
            echo "<tr><td colspan='6'>No results.</td></tr>";
        }
        ?>
    </table>
</body>
</html>
